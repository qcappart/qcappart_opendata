# README 

This repository contains the open data concerning my research. For now, only data concerning the traffic rescheduling are available. 
## Open data available 

### traffic_rescheduling
This folder contains the data related to the traffic rescheduling problem. 
Information presented here are sufficient to build the model of [CAP_RESCH_17] and to replay the experiments.
Files are detailed below:

* **stn**\_layout.txt: representation of the track layout of station **stn** (text).
* **stn**\_layout.pdf: track layout of station **stn** (figure).
* **stn**\_instance_homo\_**x**\_**n**.dat: instance n°**n** for an homogeneous traffic of **x** trains based on station **stn**.
* **stn**\_instance_homo\_**x**\_**n**.bmk: benchmark about the related instance obtained with the model of [CAP_RESCH_17].

Other data can be uploaded upon request.

## Resources

[CAP_RESCH_17] Quentin Cappart and Pierre Schaus. Rescheduling Railway Traffic on Real Time Situations using Time-Interval Variables, 2016.
## Contact

quentin.cappart@uclouvain.be